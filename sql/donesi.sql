DROP SCHEMA IF EXISTS donesi;
CREATE SCHEMA donesi DEFAULT CHARACTER SET utf8;
USE donesi;

CREATE TABLE restoran (
	idr INT AUTO_INCREMENT,
	nazivr VARCHAR(30) NOT NULL,
	otvoreno TIME NOT NULL,
	zatvoreno TIME NOT NULL,
	PRIMARY KEY (idR)
);

CREATE TABLE tipkuhinje (
	idt INT AUTO_INCREMENT,
	nazivkuhinje VARCHAR(30) NOT NULL,
	PRIMARY KEY (idT)
);

CREATE TABLE jelo(
	idj INT AUTO_INCREMENT,
	nazivjela VARCHAR(50) NOT NULL,
	cena DECIMAL (7,2) NOT NULL,
	idr INT NOT NULL,
	idt INT NOT NULL,
	PRIMARY KEY (idJ),
	
	FOREIGN KEY (idr) REFERENCES restoran(idr)
	    ON DELETE RESTRICT,
	FOREIGN KEY (idt) REFERENCES tipkuhinje(idt)
	    ON DELETE RESTRICT   
);


INSERT INTO tipkuhinje (nazivkuhinje) VALUES ("Italijanska");
INSERT INTO tipkuhinje (nazivkuhinje) VALUES ("Nacionalna");
INSERT INTO tipkuhinje (nazivkuhinje) VALUES ("Kineska");
INSERT INTO tipkuhinje (nazivkuhinje) VALUES ("Francuska");
INSERT INTO tipkuhinje (nazivkuhinje) VALUES ("Ameri�ka");
INSERT INTO tipkuhinje (nazivkuhinje) VALUES ("Meksi�ka");

INSERT INTO restoran (nazivr, otvoreno, zatvoreno) VALUES ("Foodi�", '09:00', '22:30');
INSERT INTO restoran (nazivr, otvoreno, zatvoreno) VALUES ("Alla Lanterna", '10:00', '23:30');
INSERT INTO restoran (nazivr, otvoreno, zatvoreno) VALUES ("Gusan", '00:00', '23:59');
INSERT INTO restoran (nazivr, otvoreno, zatvoreno) VALUES ("KFC Novi Sad", '10:00', '22:00');
INSERT INTO restoran (nazivr, otvoreno, zatvoreno) VALUES ("Kineski zmaj", '10:00', '21:45');
INSERT INTO restoran (nazivr, otvoreno, zatvoreno) VALUES ("Big Burito", '10:00', '21:45');


INSERT INTO jelo (nazivjela, cena, idr, idt) VALUES ("Piletina u se�uan sosu ", '450', 5, 3);
INSERT INTO jelo (nazivjela, cena, idr, idt) VALUES ("Ljuto kisela supa ", '130',5,3);
INSERT INTO jelo (nazivjela, cena, idr, idt) VALUES ("Burrito Tradicionales", '670',2,6);
INSERT INTO jelo (nazivjela, cena, idr, idt) VALUES ("Pan pica Peperoni ", '764',2,1);
INSERT INTO jelo (nazivjela, cena, idr, idt) VALUES ("Paradajz �orba", '160',3,2);
INSERT INTO jelo (nazivjela, cena, idr, idt) VALUES ("Veliki pile�i burito nacho sir", '290',6,6);
INSERT INTO jelo (nazivjela, cena, idr, idt) VALUES ("Klasik burger", '200',1,5);
INSERT INTO jelo (nazivjela, cena, idr, idt) VALUES ("Hot wings meni", '450',4,5);
INSERT INTO jelo (nazivjela, cena, idr, idt) VALUES ("Gove�a krem �orba ", '244',2,2);
