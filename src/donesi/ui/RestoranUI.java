package donesi.ui;

import java.sql.Connection;
import java.util.List;
import donesi.dao.RestoranDAO;
import donesi.model.Restoran;

public class RestoranUI {

	public static void prikazSvih(Connection conn) {
	
//		List<Restoran> restorani = RestoranDAO.getAll(conn);
//		System.out.println("=========================================================");
//		System.out.printf("|%4s|%21s|%12s|%15s|\n", "Id", "Naziv", "otvara se", "zatvara se");
//		System.out.println("=========================================================");
//		for (Restoran restoran : restorani) {
//			System.out.printf("|%4d|%21s|%12d|%15d|\n", restoran.getIdr(), restoran.getNaziv(), restoran.getOtvoreno(), restoran.getZatvoreno());
//		}
//		System.out.println("=========================================================");
		
		List<Restoran> restorani = RestoranDAO.getAll(conn);
		for (Restoran itRestoran: restorani)
			System.out.println(itRestoran);
	
	}
	
}
