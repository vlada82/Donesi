package donesi.ui;

import java.sql.Connection;
import java.util.ArrayList;

import donesi.dao.JeloDAO;
import donesi.dao.TipKuhinjeDAO;
import donesi.model.Restoran;
import donesi.model.TipKuhinje;
import utils.PomocnaKlasa;

public class TipKuhinjeUI {

	public static void ispis(Connection conn) {
		System.out.println("id\t naziv kuhinje");
		ArrayList<TipKuhinje> tipKuhinje = TipKuhinjeDAO.getAll(conn);
		for (int i = 0; i < tipKuhinje.size(); i++) {
			System.out.println(tipKuhinje.get(i));
		}
		System.out.println("Unesite id zeljene kuhinje:");
		int id = PomocnaKlasa.ocitajCeoBroj();
		
		TipKuhinje kuhinja = TipKuhinjeDAO.getById(conn, id);
		ArrayList<Restoran> restorani = JeloDAO.getRestorn(conn, kuhinja);
		for (int i = 0; i < restorani.size(); i++) {
			System.out.println(restorani.get(i));
		}
	}

}
