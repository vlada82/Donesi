package donesi.ui;

import java.sql.Connection;

import donesi.dao.JeloDAO;
import donesi.dao.RestoranDAO;
import donesi.dao.TipKuhinjeDAO;
import donesi.model.Jelo;
import donesi.model.Restoran;
import donesi.model.TipKuhinje;
import utils.PomocnaKlasa;

public class JeloUI {

	public static void unos(Connection conn) {
	
		System.out.println("Unesite naziv jela: ");
		String s = PomocnaKlasa.ocitajTekst();
		
		System.out.println("Unesite cenu jela: ");
		double cena = PomocnaKlasa.ocitajRealanBroj();
		
		System.out.println("Unesite id restorana: ");
		int idRest = PomocnaKlasa.ocitajCeoBroj();
		
		Restoran restoran = RestoranDAO.getRestoranById(conn, idRest);
		
		System.out.println("Unesite tip kuhinje jela: ");
		int idTipa = PomocnaKlasa.ocitajCeoBroj();
		
		TipKuhinje tip = TipKuhinjeDAO.getById(conn, idTipa);
		
		Jelo jelo = new Jelo (s, cena, tip, restoran);
		JeloDAO.add(conn, jelo);

		while (PomocnaKlasa.ocitajOdlukuOPotvrdi("Da li zelite da dodate jos jedno novo jelo?") != 'N') {
			unos(conn);
		}
	}

}
