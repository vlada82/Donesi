package donesi.ui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import utils.PomocnaKlasa;

public class ApplicationUI {

	public static void main(String[] args) {
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/donesi?useSSL=false", 
					"root", 
					"root");
		} catch (Exception ex) {
			System.out.println("Neuspela konekcija na bazu!");
			ex.printStackTrace();

			System.exit(0);
		}

		int odluka = -1;
		while (odluka != 0) {
			ApplicationUI.ispisiMenu();
			
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			
			switch (odluka) {
				case 0:
					System.out.println("Izlaz iz programa");
					break;
				case 1:
					RestoranUI.prikazSvih(conn);
					break;
				case 2:
					JeloUI.unos(conn);
					break;
				case 3:
					TipKuhinjeUI.ispis(conn);
					break;
				default:
					System.out.println("Nepostojeca komanda");
					break;
			}
		}
		
		try {
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	public static void ispisiMenu() {
		System.out.println("Menjacnica - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - Prikaz svih restorana");
		System.out.println("\tOpcija broj 2 - Unos novog jela");
		System.out.println("\tOpcija broj 3 - Prikaz otvorenih restorana po tipu kuhinje");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
	}

}
