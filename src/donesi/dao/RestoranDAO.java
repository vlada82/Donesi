package donesi.dao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import donesi.model.Restoran;

public class RestoranDAO {

	public static List<Restoran> getAll(Connection conn) {

		List<Restoran> restorani = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT idr, nazivr, otvoreno, zatvoreno FROM restoran";

			stmt = conn.createStatement();

			rset = stmt.executeQuery(query);
			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String nazivR = rset.getString(index++);
				Time otvoreno = rset.getTime(index++);
				Time zatvoreno = rset.getTime(index++);


				Restoran restoran = new Restoran (id,nazivR,otvoreno, zatvoreno);
				restorani.add(restoran);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				rset.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return restorani;
	}

	public static Restoran getRestoranById(Connection conn, int idRest) {

		Restoran restoran = null;

		Statement stmt = null;
		ResultSet rset = null;

		try {
			String query = "SELECT idr, nazivr, otvoreno, zatvoreno FROM restoran WHERE idr = " + idRest;
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while(rset.next()) {
				int index = 1;
				int idr = rset.getInt(index++);
				String naziv = rset.getString(index++);
				Time otvoreno = rset.getTime(index++);
				Time zatvoreno = rset.getTime(index++);

				restoran = new Restoran(idr, naziv, otvoreno, zatvoreno);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}


		return restoran;
	}

}
