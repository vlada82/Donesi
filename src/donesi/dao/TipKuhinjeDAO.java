package donesi.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import donesi.model.TipKuhinje;

public class TipKuhinjeDAO {

	public static TipKuhinje getById(Connection conn, int idTipa) {
		TipKuhinje tipKuhinje = null;
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT nazivkuhinje FROM tipkuhinje WHERE idt = " + idTipa;
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				int index = 1;
				String nazivKuhinje = rset.getString(index++);
				
				tipKuhinje = new TipKuhinje(idTipa, nazivKuhinje);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		
		return tipKuhinje;
	}

	public static ArrayList<TipKuhinje> getAll(Connection conn) {
		
		ArrayList<TipKuhinje> lista = new ArrayList<>();
		//List<Soba> sobe = new ArrayList<>();
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT * FROM tipkuhinje";
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String nazivkuhinje = rset.getString(index++);
				
				TipKuhinje listaKuh = new TipKuhinje(id, nazivkuhinje);
				lista.add(listaKuh);
			}
			
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return lista;
	}

}
