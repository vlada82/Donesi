package donesi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;

import donesi.model.Jelo;
import donesi.model.Restoran;
import donesi.model.TipKuhinje;

public class JeloDAO {

public static ArrayList<Restoran> getRestorn(Connection conn, TipKuhinje kuhinja) {
		
		ArrayList<Restoran> lista = new ArrayList<>();
		//List<Soba> sobe = new ArrayList<>();
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT r.idr,r.nazivr,r.otvoreno,r.zatvoreno FROM jelo JOIN tipkuhinje ON tipkuhinje.idt = jelo.idt JOIN restoran r ON r.idr= tipkuhinje.idt WHERE tipkuhinje.idt = "+kuhinja.getIdt();
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String naziv = rset.getString(index++);
				Time otvoreno = rset.getTime(index++);
				Time zatvoreno = rset.getTime(index++);
				
				Restoran temp = new Restoran(id,naziv,otvoreno,zatvoreno);
			
//				TipKuhinje listaKuh = new TipKuhinje(id, nazivkuhinje);
				lista.add(temp);
			}
			
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return lista;
	}

	public static boolean add(Connection conn, Jelo jelo) {
		
		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO jelo (nazivjela, cena, idr, idt) VALUES (?,?,?,?)";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, jelo.getNazivJela());
			pstmt.setDouble(2, jelo.getCena());
			pstmt.setInt(3, jelo.getRestoran().getIdr());
			pstmt.setInt(4, jelo.getTipKuhinje().getIdt());

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		return false;
		
	}

	

}
