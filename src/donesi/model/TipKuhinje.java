package donesi.model;

public class TipKuhinje {
	
	private int idt;
	private String nazivKuhinje;
	
	public TipKuhinje() {
	}

	public TipKuhinje(int idt, String nazivKuhinje) {
		super();
		this.idt = idt;
		this.nazivKuhinje = nazivKuhinje;
	}

	public int getIdt() {
		return idt;
	}

	public void setIdt(int idt) {
		this.idt = idt;
	}

	public String getNazivKuhinje() {
		return nazivKuhinje;
	}

	public void setNazivKuhinje(String nazivKuhinje) {
		this.nazivKuhinje = nazivKuhinje;
	}

	@Override
	public String toString() {
		return idt + " \t" + nazivKuhinje ;
	}
	
	

}
