package donesi.model;

import java.sql.Time;
import java.util.ArrayList;

public class Restoran {
	
	private int idr;
	private String naziv;
	private Time otvoreno;
	private Time zatvoreno;
	
	private ArrayList<Jelo> jela = new ArrayList<>();

	public Restoran() {
		}

	public Restoran(int idr, String naziv, Time otvoreno, Time zatvoreno, ArrayList<Jelo> jela) {
		super();
		this.idr = idr;
		this.naziv = naziv;
		this.otvoreno = otvoreno;
		this.zatvoreno = zatvoreno;
		this.jela = jela;
	}

	public Restoran(int idr, String naziv, Time otvoreno, Time zatvoreno) {
		super();
		this.idr = idr;
		this.naziv = naziv;
		this.otvoreno = otvoreno;
		this.zatvoreno = zatvoreno;
	}

	public int getIdr() {
		return idr;
	}

	public void setIdr(int idr) {
		this.idr = idr;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Time getOtvoreno() {
		return otvoreno;
	}

	public void setOtvoreno(Time otvoreno) {
		this.otvoreno = otvoreno;
	}

	public Time getZatvoreno() {
		return zatvoreno;
	}

	public void setZatvoreno(Time zatvoreno) {
		this.zatvoreno = zatvoreno;
	}

	public ArrayList<Jelo> getJela() {
		return jela;
	}

	public void setJela(ArrayList<Jelo> jela) {
		this.jela = jela;
	}

	@Override
	public String toString() {
		return  idr + " " + naziv + " " + otvoreno + " do " + zatvoreno;
	}
	
	

}

