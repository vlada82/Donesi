package donesi.model;

public class Jelo {
	
	private int idj;
	private String nazivJela;
	private double cena;
	private TipKuhinje tipKuhinje;
	private Restoran restoran;
	
	public Jelo() {
		
	}


	public Jelo(int idj, String nazivJela, double cena, TipKuhinje tipKuhinje) {
		super();
		this.idj = idj;
		this.nazivJela = nazivJela;
		this.cena = cena;
		this.tipKuhinje = tipKuhinje;
	}


	public Jelo(String nazivJela, double cena, TipKuhinje tipKuhinje, Restoran restoran) {
		super();
		this.nazivJela = nazivJela;
		this.cena = cena;
		this.tipKuhinje = tipKuhinje;
		this.restoran = restoran;
	}


	public Jelo(String s, double cena2, int idTipa) {
		// TODO Auto-generated constructor stub
	}


	public TipKuhinje getTipKuhinje() {
		return tipKuhinje;
	}


	public void setTipKuhinje(TipKuhinje tipKuhinje) {
		this.tipKuhinje = tipKuhinje;
	}


	public int getIdj() {
		return idj;
	}

	public void setIdj(int idj) {
		this.idj = idj;
	}

	public String getNazivJela() {
		return nazivJela;
	}

	public void setNazivJela(String nazivJela) {
		this.nazivJela = nazivJela;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public Restoran getRestoran() {
		return restoran;
	}

	public void setRestoran(Restoran restoran) {
		this.restoran = restoran;
	}

	@Override
	public String toString() {
		return "Jelo [idj=" + idj + ", nazivJela=" + nazivJela + ", cena=" + cena + ", tipKuhinje=" + tipKuhinje
				+ ", restoran=" + restoran + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idj;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Jelo other = (Jelo) obj;
		if (idj != other.idj)
			return false;
		return true;
	}
	

}
